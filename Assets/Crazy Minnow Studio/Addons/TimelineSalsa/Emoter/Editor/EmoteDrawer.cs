using UnityEditor;
using UnityEngine;

namespace CrazyMinnow.SALSA.Timeline
{
    [CustomPropertyDrawer(typeof(EmoteBehavior))]
    public class EmoteDrawer : PropertyDrawer
    {
        private float lineMultiplier = 1.2f;
        int fieldCount = 0;

        public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
        {
            return fieldCount * lineMultiplier * EditorGUIUtility.singleLineHeight;
        }

        public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty emoteNameProperty = property.FindPropertyRelative("emoteName");
            SerializedProperty emoteAnimateOnProperty = property.FindPropertyRelative("animateOn");
            SerializedProperty emoteHandlerProperty = property.FindPropertyRelative("handler");
            SerializedProperty emoteDynamicsProperty = property.FindPropertyRelative("dynamics");
            SerializedProperty emoteDynamicsRangeProperty = property.FindPropertyRelative("dynamicsAmount");
            SerializedProperty emoteDynamicsTimingProperty = property.FindPropertyRelative("dynamicsTiming");

            var singleFieldRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
            EditorGUI.PropertyField(singleFieldRect, emoteNameProperty);
            fieldCount = 2;

            singleFieldRect.y += EditorGUIUtility.singleLineHeight * lineMultiplier;
            EditorGUI.PropertyField(singleFieldRect, emoteHandlerProperty);

            if (emoteHandlerProperty.enumValueIndex == 0)
            {
                fieldCount = 4;
                singleFieldRect.y += EditorGUIUtility.singleLineHeight * lineMultiplier;
                EditorGUI.PropertyField(singleFieldRect, emoteAnimateOnProperty);

                if (emoteAnimateOnProperty.boolValue)
                {
                    fieldCount = 5;
                    singleFieldRect.y += EditorGUIUtility.singleLineHeight * lineMultiplier;
                    EditorGUI.PropertyField(singleFieldRect, emoteDynamicsProperty);

                    if (emoteDynamicsProperty.boolValue)
                    {
                        fieldCount = 7;
                        singleFieldRect.y += EditorGUIUtility.singleLineHeight * lineMultiplier;
                        EditorGUI.PropertyField(singleFieldRect, emoteDynamicsRangeProperty);

                        singleFieldRect.y += EditorGUIUtility.singleLineHeight * lineMultiplier;
                        EditorGUI.PropertyField(singleFieldRect, emoteDynamicsTimingProperty);
                    }

                    singleFieldRect.y += EditorGUIUtility.singleLineHeight * lineMultiplier;
                    EditorGUI.LabelField(singleFieldRect, new GUIContent("NOTE: Turns on an emote...", "Add an additional emote clip to the track and un-check 'Animate On' to turn the clip off."));
                }
                else
                {
                    singleFieldRect.y += EditorGUIUtility.singleLineHeight * lineMultiplier;
                    EditorGUI.LabelField(singleFieldRect, new GUIContent("NOTE: Turns an emote off...", "Requires a previous emote clip on the track to turn the emote on with 'Animate On' enabled."));
                }
            }
            else
            {
                fieldCount = 3;
                singleFieldRect.y += EditorGUIUtility.singleLineHeight * lineMultiplier;
                EditorGUI.LabelField(singleFieldRect, new GUIContent("NOTE: Turns an emote on and off...", "The leading edge of the clip turns the emote on and the emote will end by the trailing edge."));
            }

            // Find the clip this behaviour is associated with
            EmoteClip clip = property.serializedObject.targetObject as EmoteClip;
            if (!clip) return;

            var shapeClip = clip.template;
            if (shapeClip != null)
            {
                // Assume that the currently selected object is the internal class UnityEditor.Timeline.EditorClip
                // this gives you access to the clip start, duration etc.
                SerializedObject editorGUI = new SerializedObject(Selection.activeObject);

                // Grab the clip title, set new title
                SerializedProperty title = editorGUI.FindProperty("m_Clip.m_DisplayName");
                if (title == null)
                    title = editorGUI.FindProperty("m_Item.m_DisplayName");

                if (title != null)
                {
                    title.stringValue = (shapeClip.handler == ExpressionComponent.ExpressionHandler.OneWay ?
                             (shapeClip.animateOn ? "> " + shapeClip.emoteName : "< " + shapeClip.emoteName)
                             : "> " + shapeClip.emoteName + " <");
                }

                editorGUI.ApplyModifiedProperties();
            }
        }
    }
}