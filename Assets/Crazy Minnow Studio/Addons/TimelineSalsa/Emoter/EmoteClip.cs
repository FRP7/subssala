using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace CrazyMinnow.SALSA.Timeline
{
    [Serializable]
    public class EmoteBehavior : PlayableBehaviour
    {
        // We only need to define a single field for this Behavior
        public string emoteName;
        public bool animateOn = true;
        public ExpressionComponent.ExpressionHandler handler = ExpressionComponent.ExpressionHandler.RoundTrip;
        public bool dynamics = false;
        [Range(0.0f, 1.0f)] public float dynamicsAmount = 1.0f;
        [Range(0.0f, 1.0f)] public float dynamicsTiming = .2f;
    }

    [Serializable]
    public class EmoteClip : PlayableAsset, ITimelineClipAsset
    {
        public EmoteBehavior template = new EmoteBehavior();

        // Create the runtime version of the clip, by creating a copy of the template
        public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
        {
            return ScriptPlayable<EmoteBehavior>.Create (graph, template);
        }

        // Use this to tell the Timeline Editor what features this clip supports
        public ClipCaps clipCaps
        {
            get { return ClipCaps.None; }
        }
    }
}
