using UnityEngine;
using UnityEngine.Playables;
using Random = UnityEngine.Random;

namespace CrazyMinnow.SALSA.Timeline
{
    public class EmoteMixer : PlayableBehaviour
    {
        private int prevHandleID = -1; // track handle id to eliminate duplicate playback of the same track clip.
        private float timecheck;
        private bool playDynamics = false;
        private bool playAnimDirection = true;
        private float playVariAmount = 0.0f;
        private float playVariTiming = 0.0f;
        private string playVariShape = "";
        private string playEmote = ""; // the name of the emote group to trigger.
        private ExpressionComponent.ExpressionHandler playHandler = ExpressionComponent.ExpressionHandler.RoundTrip;

        private const bool ANIMATE_ON = true;
        private const bool ANIMATE_OFF = false;

        // NOTE: This function is called at runtime and edit time.  Keep that in mind when setting the values of properties.
        public override void ProcessFrame(Playable playableHandle, FrameData info, object playerData)
        {
            var trackBinding = playerData as Emoter;

            if (!trackBinding)
                return; // no RandomEyes instance linked...bail

            int count = playableHandle.GetInputCount(); // how many clips on the track.
            float playDuration = 0.0f; // will hold the track clip's duration.

            // let's loop through the clips on the track to find the one currently under the playhead.
            for (int i = 0; i < count; i++)
            {
                var inputHandle = playableHandle.GetInput(i);
                var weight = playableHandle.GetInputWeight(i);

                if (inputHandle.IsValid() && inputHandle.GetPlayState() == PlayState.Playing)
                {
                    var inputData = ((ScriptPlayable<EmoteBehavior>) inputHandle).GetBehaviour();
                    if (inputData != null)
                    {
                        playHandler = inputData.handler;
                        if (playHandler == ExpressionComponent.ExpressionHandler.RoundTrip)
                        {
                            playAnimDirection = ANIMATE_ON;
                            playDynamics = false;
                        }
                        else
                            playAnimDirection = inputData.animateOn;

                        playEmote = inputData.emoteName;

                        playVariTiming = inputData.dynamicsTiming;

                        // animating off = turn off dynamics
                        if (playAnimDirection == ANIMATE_OFF)
                            playDynamics = false;
                        else
                            playDynamics = inputData.dynamics;

                        playVariShape = playDynamics ? playEmote : "";
                        playVariAmount = inputData.dynamicsAmount;

                        if (weight > 0.95f)
                            playDuration = (float) inputHandle.GetDuration();
                    }

                    // activate the emote group with a computed duration based on the clip graph length.
                    if (playEmote != "" && prevHandleID != i) // we have a 'valid' emote group name to use.
                    {
                        if (Application.isPlaying)
                        {
                            trackBinding.ManualEmote(playEmote, playHandler, playDuration, playAnimDirection);
                        }
//                        Debug.Log(playEmote + " " + playHandler + " " + playDuration + " " + playAnimDirection + " dyn?" + playDynamics + " ID: " + i + " prev: " + prevHandleID);
                    }

                    prevHandleID = i; // set the current handle id.

                    break; // since we've found and processed the correct clip...bail the loop.
                }
            }

            if (playHandler == ExpressionComponent.ExpressionHandler.OneWay && playDynamics && Time.time - timecheck > playVariTiming && Application.isPlaying)
            {
                timecheck = Time.time;
                var frac = Random.Range(playVariAmount - 1.0f, 1.0f);
                trackBinding.ManualEmote(playVariShape, playHandler, playDuration, playAnimDirection, frac);
            }
        }
    }
}