using UnityEditor;
using UnityEngine;

namespace CrazyMinnow.SALSA.Timeline
{
    [CustomPropertyDrawer(typeof(SalsaAudioBehaviour))]
    public class SalsaAudioDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
        {
            int fieldCount = 1;
            return fieldCount * EditorGUIUtility.singleLineHeight;
        }
    
        public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty salsaAudioClipProp = property.FindPropertyRelative("salsaAudioClip");
    
            var singleFieldRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
            EditorGUI.PropertyField(singleFieldRect, salsaAudioClipProp);
            
            // Find the clip this behaviour is associated with
            SalsaAudioClip clip = property.serializedObject.targetObject as SalsaAudioClip;
            if (!clip) return;
            
            var audioClip = clip.template.salsaAudioClip;
            if(audioClip)
            {
                // Assume that the currently selected object is the internal class UnityEditor.Timeline.EditorClip
                // this gives you access to the clip start, duration etc.
                SerializedObject editorGUI = new SerializedObject(Selection.activeObject);
     
                // Grab the duration, set new duration
                SerializedProperty duration = editorGUI.FindProperty("m_Clip.m_Duration");
                if (duration == null)
                    duration = editorGUI.FindProperty("m_Item.m_Duration");

                if (duration != null)
                {
                    duration.doubleValue = (double) audioClip.samples / audioClip.frequency;
                    duration.doubleValue -= .01; // safety net for clip display -- tries to avoid double firing of clips
                }
                
                // Grab the clip title, set new title
                SerializedProperty title = editorGUI.FindProperty("m_Clip.m_DisplayName");
                if (title == null)
                    title = editorGUI.FindProperty("m_Item.m_DisplayName");

                if (title != null)
                {
                    title.stringValue = audioClip.name;
                }
                
                editorGUI.ApplyModifiedProperties();
            }
        }
    }
}