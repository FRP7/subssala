using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace CrazyMinnow.SALSA.Timeline
{
    [Serializable]
    public class SalsaAudioBehaviour : PlayableBehaviour
    {
        // We only need to define a single field for this Behavior
        public AudioClip salsaAudioClip;
    }

    [Serializable]
    public class SalsaAudioClip : PlayableAsset, ITimelineClipAsset
    {
        public SalsaAudioBehaviour template = new SalsaAudioBehaviour ();

        // Create the runtime version of the clip, by creating a copy of the template
        public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
        {
            return ScriptPlayable<SalsaAudioBehaviour>.Create (graph, template);
        }

        // Use this to tell the Timeline Editor what features this clip supports
        public ClipCaps clipCaps
        {
            get { return ClipCaps.None; }
        }

        // Adjust the default/reset computation of duration to match the length of the AudioClip
        public override double duration
        {
            get
            {
                if (template.salsaAudioClip == null)
                    return base.duration;

                return (double)template.salsaAudioClip.samples / (double)template.salsaAudioClip.frequency;
            }
        }
    }
}
