using UnityEngine;
using UnityEngine.Playables;

namespace CrazyMinnow.SALSA.Timeline
{
    public class SalsaAudioMixer : PlayableBehaviour
    {
        private int prevHandleID = -1;       // clip handle id to eliminate duplicate playback of the same track clip.
        private int currHandleID = -2;       // retains the "playing" clip in instances of pause/resume callbacks.
        private AudioSource trackBinding;    // global trackBinding reference for pause/resume callbacks.

        // NOTE: This function is called at runtime and edit time.  Keep that in mind when setting the values of properties.
        public override void ProcessFrame(Playable playableHandle, FrameData info, object playerData)
        {
            trackBinding = playerData as AudioSource;

            if (!trackBinding)
                return;

            AudioClip audioClip = null;
            int count = playableHandle.GetInputCount();

            for (int i = 0; i < count; i++)
            {
                currHandleID = i; // retain the current clip for pause/resume callbacks.
                var inputHandle = playableHandle.GetInput(i);
                var weight = playableHandle.GetInputWeight(i);

                if (inputHandle.IsValid() && inputHandle.GetPlayState() == PlayState.Playing && weight > 0)
                {
                    var inputData = ((ScriptPlayable<SalsaAudioBehaviour>) inputHandle).GetBehaviour();
                    if (inputData != null)
                    {
                        // blending doesn't make sense for lip-sync, we're not allowing it.
                        if (weight > 0.95f)
                        {
                            audioClip = inputData.salsaAudioClip;
                        }
                    }

                    // we've found an active clip and grabbed the data from the behavior clip
                    //     -- now use it.
                    // additionally, confirm the previous handle id is not the same as the
                    //     current id -- prevents duplicate play if the duration calculation is
                    //     incorrect based on float err.
                    if (audioClip != null && prevHandleID != i)
                    {
                        if (trackBinding != null)
                        {
                            trackBinding.clip = audioClip;
                            // we're only going to play if the Unity "player" is playing.
                            // eliminates audio playing in design-time.
                            if (!trackBinding.isPlaying && Application.isPlaying)
                            {
                                trackBinding.Play();
                            }
                        }
                    }

                    prevHandleID = i; // set the current handle id.

                    break;
                }

                currHandleID = -2; // reset the current clip for pause/resume callbacks, prevents the last clip from playing if pause/resume on empty space (no clip)
            }
        }

        public override void OnBehaviourPause(Playable playableHandle, FrameData info)
        {
            if (trackBinding != null)
                trackBinding.Pause();
        }

        public override void OnBehaviourPlay(Playable playable, FrameData info)
        {
            if (trackBinding != null && prevHandleID == currHandleID)
                trackBinding.Play();
        }
    }
}