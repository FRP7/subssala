using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace CrazyMinnow.SALSA.Timeline
{
    [TrackColor(0.1764706f, 0.4039216f, 0.6980392f)]
    [TrackClipType(typeof(SalsaAudioClip))]
    [TrackBindingType(typeof(AudioSource))]
    public class SalsaAudioTrack : TrackAsset
    {
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            return ScriptPlayable<SalsaAudioMixer>.Create (graph, inputCount);
        }
    }
}
