﻿using System.Collections;
using UnityEngine;

public class Audios : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField] AudioSource audioClient;
    [SerializeField] AudioClip[] clipsClient;

#pragma warning restore 0649

    ////////////////////////////////////// CLIENT AUDIO //////////////////////////////////////
    public void Motive_Audio() => StartCoroutine(Motive());
    private IEnumerator Motive()
    {
        yield return new WaitForSeconds(1);
        GetComponent<Subtitles>().Motive_Subs();
        audioClient.clip = clipsClient[0];
        audioClient.Play();
        yield return new WaitForSeconds(audioClient.clip.length);
        yield return new WaitForSeconds(1);
    }

    public void Info1_A_Audio() => StartCoroutine(Info1_A());
    private IEnumerator Info1_A()
    {
        yield return new WaitForSeconds(1);
        GetComponent<Subtitles>().Info1_A_Subs();
        audioClient.clip = clipsClient[1];
        audioClient.Play();
        yield return new WaitForSeconds(audioClient.clip.length);
        yield return new WaitForSeconds(1);

    }

    public void Info1_B_Audio() => StartCoroutine(Info1_B());
    private IEnumerator Info1_B()
    {
        yield return new WaitForSeconds(1);
        GetComponent<Subtitles>().Info1_B_Subs();
        audioClient.clip = clipsClient[2];
        audioClient.Play();
        yield return new WaitForSeconds(audioClient.clip.length);
        yield return new WaitForSeconds(1);

    }

    public void CallParentsAudio() => StartCoroutine(CallParent());
    private IEnumerator CallParent() {
        yield return new WaitForSeconds(1);
        GetComponent<Subtitles>().Diana_Ethics_CallParents();
        audioClient.clip = clipsClient[4];
        audioClient.Play();
        yield return new WaitForSeconds(audioClient.clip.length);
        yield return new WaitForSeconds(1);

    }

    public void CallComissionAudio() => StartCoroutine(CallComission());
    private IEnumerator CallComission() {
        yield return new WaitForSeconds(1);
        GetComponent<Subtitles>().Diana_Ethics_CallComission();
        audioClient.clip = clipsClient[5];
        audioClient.Play();
        yield return new WaitForSeconds(audioClient.clip.length);
        yield return new WaitForSeconds(1);

    }

    public void EthicsAudio() => StartCoroutine(EthicsTalk());
    private IEnumerator EthicsTalk() {
        yield return new WaitForSeconds(1);
        GetComponent<Subtitles>().Diana_Ethics_Talk();
        audioClient.clip = clipsClient[6];
        audioClient.Play();
        yield return new WaitForSeconds(audioClient.clip.length);
        yield return new WaitForSeconds(1);

    }

    public void Feed1ExpAudio() => StartCoroutine(Feed1Exp());
    private IEnumerator Feed1Exp() {
        yield return new WaitForSeconds(1);
        GetComponent<Subtitles>().Instructions_Feed1_Exp();
        audioClient.clip = clipsClient[7];
        audioClient.Play();
        yield return new WaitForSeconds(audioClient.clip.length);
        yield return new WaitForSeconds(1);

    }

    public void Feed2Audio() => StartCoroutine(Feed2());
    private IEnumerator Feed2() {
        yield return new WaitForSeconds(1);
        GetComponent<Subtitles>().Instructions_Feed2();
        audioClient.clip = clipsClient[8];
        audioClient.Play();
        yield return new WaitForSeconds(audioClient.clip.length);
        yield return new WaitForSeconds(1);

    }

    public void Feed3Audio() => StartCoroutine(Feed3());
    private IEnumerator Feed3() {
        yield return new WaitForSeconds(1);
        GetComponent<Subtitles>().Instructions_Feed3();
        audioClient.clip = clipsClient[9];
        audioClient.Play();
        yield return new WaitForSeconds(audioClient.clip.length);
        yield return new WaitForSeconds(1);

    }

    public void Feed4Audio() => StartCoroutine(Feed4());
    private IEnumerator Feed4() {
        yield return new WaitForSeconds(1);
        GetComponent<Subtitles>().Instructions_Feed4();
        audioClient.clip = clipsClient[10];
        audioClient.Play();
        yield return new WaitForSeconds(audioClient.clip.length);
        yield return new WaitForSeconds(1);

    }

    public void Feed5Audio() => StartCoroutine(Feed5());
    private IEnumerator Feed5() {
        yield return new WaitForSeconds(1);
        GetComponent<Subtitles>().Instructions_Feed5();
        audioClient.clip = clipsClient[11];
        audioClient.Play();
        yield return new WaitForSeconds(audioClient.clip.length);
        yield return new WaitForSeconds(1);

    }

    public void ThanksAudio() => StartCoroutine(Thanks());
    private IEnumerator Thanks() {
        yield return new WaitForSeconds(1);
        GetComponent<Subtitles>().Instructions_Thanks();
        audioClient.clip = clipsClient[12];
        audioClient.Play();
        yield return new WaitForSeconds(audioClient.clip.length);
        yield return new WaitForSeconds(1);

    }
}
