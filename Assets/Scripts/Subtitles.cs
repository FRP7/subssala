﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Subtitles : MonoBehaviour
{
#pragma warning disable 0649
     //[SerializeField] TextMeshProUGUI subs;
    [SerializeField] Text subs; //test
    //[SerializeField] TextMeshPro subs;
    [SerializeField] SkinnedMeshRenderer skinnedmeshrenderer;
#pragma warning restore 0649

    ////////////////////////////////////// SetBlendShapeWeight(int index, float value);
    public void Motive_Subs() => StartCoroutine(Motive());
    private IEnumerator Motive()
    {
        yield return new WaitForSeconds(1);
        subs.text = "Subtítulos Motivo";
        yield return new WaitForSeconds(2);
        subs.text = "";

        //yield return new WaitForSeconds(1);
        //subs.text = "A minha irmã ligou-me ontem a dizer que já tinha falado com os meus pais, que já sabe que este ano devo chumbar, que a directora de turma lhes disse que não tenho ido às aulas";
        //yield return new WaitForSeconds(10);
        //subs.text = "";
        //yield return new WaitForSeconds(.5f);
        //subs.text = "E depois disse: 'Ah, porque é que não vens passar o fim-de-semana connosco, ia fazer-te bem mudar de ares'";
        //yield return new WaitForSeconds(5.5f);
        //subs.text = "";
        //yield return new WaitForSeconds(.5f);
        //subs.text = "E aquele gajo, o marido dela, porque eles estavam em alta voz, começa do outro lado: 'Ah sim, sim, vem, vem, ah vais ver que vais gostar de tar aqui' e tal";
        //yield return new WaitForSeconds(9);
        //subs.text = "";
        //yield return new WaitForSeconds(.5f);
        //subs.text = "Tipo, que é isto? E ainda por cima depois daquilo que ele fez…";
        //yield return new WaitForSeconds(3.5f);
        //subs.text = "";
        //yield return new WaitForSeconds(.5f);
        //subs.text = "À noite não conseguia dormir, tive na net a ler coisas sobre miúdas que também foram violadas, tipo, por pais, por irmãos, tios e ya, e cunhados";
        //yield return new WaitForSeconds(8.6f);
        //subs.text = "";
        //yield return new WaitForSeconds(.2f);
        //subs.text = "E há bué bué gente a passar por isto. Se a minha irmã soubesse na volta ia culpar-me a mim";
        //yield return new WaitForSeconds(5.2f);
        //subs.text = "";
        //yield return new WaitForSeconds(.5f);
        //subs.text = "E é o que ele diz, que ia achar que fui eu que fui ter com ele, que eu é que queria. Ele até diz que me mata se eu disser isto a alguém";
        //yield return new WaitForSeconds(8.5f);
        //subs.text = "";
    }

    public void Info1_A_Subs() => StartCoroutine(Info1_A());
    private IEnumerator Info1_A()
    {
        yield return new WaitForSeconds(1);
        subs.text = "Subtítulos Info1";
        yield return new WaitForSeconds(2);
        subs.text = "";
        //yield return new WaitForSeconds(.5f);
    }

    public void Info1_B_Subs() => StartCoroutine(Info1_B());
    /*private IEnumerator Info1_B()
    {
        skinnedmeshrenderer.SetBlendShapeWeight(0, 0); //Blink_Left
        skinnedmeshrenderer.SetBlendShapeWeight(1, 0); //Blink_Right
        skinnedmeshrenderer.SetBlendShapeWeight(2, 50); //BrowsDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(3, 50); //BrowsDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(4, 0); //BrowsIn_Left
        skinnedmeshrenderer.SetBlendShapeWeight(5, 0); //BrowsIn_Right
        skinnedmeshrenderer.SetBlendShapeWeight(6, 0); //BrowsOuterLower_Left
        skinnedmeshrenderer.SetBlendShapeWeight(7, 0); //BrowsOuterLower_Right
        skinnedmeshrenderer.SetBlendShapeWeight(8, 0); //BrowsUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(9, 0); //BrowsUp_Right
        skinnedmeshrenderer.SetBlendShapeWeight(10, 0); //CheekPuff_Left
        skinnedmeshrenderer.SetBlendShapeWeight(11, 0); //CheekPuff_Right
        skinnedmeshrenderer.SetBlendShapeWeight(12, 0); //EyeWide_Left
        skinnedmeshrenderer.SetBlendShapeWeight(13, 0); //EyeWide_Right
        skinnedmeshrenderer.SetBlendShapeWeight(14, 0); //Frown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(15, 0); //Frown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(16, 0); //JawBackward
        skinnedmeshrenderer.SetBlendShapeWeight(17, 0); //JawForeward
        skinnedmeshrenderer.SetBlendShapeWeight(18, 0); //JawRotateY_Left
        skinnedmeshrenderer.SetBlendShapeWeight(19, 0); //JawRotateY_Right
        skinnedmeshrenderer.SetBlendShapeWeight(20, 0); //JawRotateZ_Left
        skinnedmeshrenderer.SetBlendShapeWeight(21, 0); //JawRotateZ_Right
        skinnedmeshrenderer.SetBlendShapeWeight(22, 0); //Jaw_Down
        skinnedmeshrenderer.SetBlendShapeWeight(23, 0); //Jaw_Left
        skinnedmeshrenderer.SetBlendShapeWeight(24, 0); //Jaw_Right
        skinnedmeshrenderer.SetBlendShapeWeight(25, 100); //Jaw_Up
        skinnedmeshrenderer.SetBlendShapeWeight(26, 0); //LowerLipDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(27, 0); //LowerLipDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(28, 0); //LowerLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(29, 0); //LowerLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(30, 0); //Midmouth_Left
        skinnedmeshrenderer.SetBlendShapeWeight(31, 0); //Midmouth_Right
        skinnedmeshrenderer.SetBlendShapeWeight(32, 0); //MouthDown
        skinnedmeshrenderer.SetBlendShapeWeight(33, 0); //MouthNarrow_Left
        skinnedmeshrenderer.SetBlendShapeWeight(34, 0); //MouthNarrow_Right
        skinnedmeshrenderer.SetBlendShapeWeight(35, 0); //MouthOpen
        skinnedmeshrenderer.SetBlendShapeWeight(36, 0); //MouthUp
        skinnedmeshrenderer.SetBlendShapeWeight(37, 0); //MouthWhistle_Narrow_Adjust_Left
        skinnedmeshrenderer.SetBlendShapeWeight(38, 0); //MouthWhitstle_Narrow_Adjust_Right
        skinnedmeshrenderer.SetBlendShapeWeight(39, 0); //NoseScrunch_Left
        skinnedmeshrenderer.SetBlendShapeWeight(40, 0); //NoseScrunch_Right
        skinnedmeshrenderer.SetBlendShapeWeight(41, 0); //Smile_Left
        skinnedmeshrenderer.SetBlendShapeWeight(42, 0); //Smile_Right
        skinnedmeshrenderer.SetBlendShapeWeight(43, 100); //Squint_Left
        skinnedmeshrenderer.SetBlendShapeWeight(44, 100); //Squint_Right
        skinnedmeshrenderer.SetBlendShapeWeight(45, 0); //TongueUp
        skinnedmeshrenderer.SetBlendShapeWeight(46, 0); //UpperLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(47, 0); //UpperLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(48, 0); //UpperLipUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(49, 0); //UpperLipUp_Right

        yield return new WaitForSeconds(3.5f);
        skinnedmeshrenderer.SetBlendShapeWeight(0, 0); //Blink_Left
        skinnedmeshrenderer.SetBlendShapeWeight(1, 0); //Blink_Right
        skinnedmeshrenderer.SetBlendShapeWeight(2, 0); //BrowsDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(3, 0); //BrowsDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(4, 0); //BrowsIn_Left
        skinnedmeshrenderer.SetBlendShapeWeight(5, 0); //BrowsIn_Right
        skinnedmeshrenderer.SetBlendShapeWeight(6, 0); //BrowsOuterLower_Left
        skinnedmeshrenderer.SetBlendShapeWeight(7, 0); //BrowsOuterLower_Right
        skinnedmeshrenderer.SetBlendShapeWeight(8, 0); //BrowsUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(9, 0); //BrowsUp_Right
        skinnedmeshrenderer.SetBlendShapeWeight(10, 0); //CheekPuff_Left
        skinnedmeshrenderer.SetBlendShapeWeight(11, 0); //CheekPuff_Right
        skinnedmeshrenderer.SetBlendShapeWeight(12, 0); //EyeWide_Left
        skinnedmeshrenderer.SetBlendShapeWeight(13, 0); //EyeWide_Right
        skinnedmeshrenderer.SetBlendShapeWeight(14, 0); //Frown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(15, 0); //Frown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(16, 0); //JawBackward
        skinnedmeshrenderer.SetBlendShapeWeight(17, 0); //JawForeward
        skinnedmeshrenderer.SetBlendShapeWeight(18, 0); //JawRotateY_Left
        skinnedmeshrenderer.SetBlendShapeWeight(19, 0); //JawRotateY_Right
        skinnedmeshrenderer.SetBlendShapeWeight(20, 0); //JawRotateZ_Left
        skinnedmeshrenderer.SetBlendShapeWeight(21, 0); //JawRotateZ_Right
        skinnedmeshrenderer.SetBlendShapeWeight(22, 0); //Jaw_Down
        skinnedmeshrenderer.SetBlendShapeWeight(23, 0); //Jaw_Left
        skinnedmeshrenderer.SetBlendShapeWeight(24, 0); //Jaw_Right
        skinnedmeshrenderer.SetBlendShapeWeight(25, 100); //Jaw_Up
        skinnedmeshrenderer.SetBlendShapeWeight(26, 0); //LowerLipDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(27, 0); //LowerLipDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(28, 0); //LowerLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(29, 0); //LowerLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(30, 0); //Midmouth_Left
        skinnedmeshrenderer.SetBlendShapeWeight(31, 0); //Midmouth_Right
        skinnedmeshrenderer.SetBlendShapeWeight(32, 0); //MouthDown
        skinnedmeshrenderer.SetBlendShapeWeight(33, 0); //MouthNarrow_Left
        skinnedmeshrenderer.SetBlendShapeWeight(34, 0); //MouthNarrow_Right
        skinnedmeshrenderer.SetBlendShapeWeight(35, 0); //MouthOpen
        skinnedmeshrenderer.SetBlendShapeWeight(36, 0); //MouthUp
        skinnedmeshrenderer.SetBlendShapeWeight(37, 0); //MouthWhistle_Narrow_Adjust_Left
        skinnedmeshrenderer.SetBlendShapeWeight(38, 0); //MouthWhitstle_Narrow_Adjust_Right
        skinnedmeshrenderer.SetBlendShapeWeight(39, 0); //NoseScrunch_Left
        skinnedmeshrenderer.SetBlendShapeWeight(40, 0); //NoseScrunch_Right
        skinnedmeshrenderer.SetBlendShapeWeight(41, 0); //Smile_Left
        skinnedmeshrenderer.SetBlendShapeWeight(42, 0); //Smile_Right
        skinnedmeshrenderer.SetBlendShapeWeight(43, 50); //Squint_Left
        skinnedmeshrenderer.SetBlendShapeWeight(44, 50); //Squint_Right
        skinnedmeshrenderer.SetBlendShapeWeight(45, 0); //TongueUp
        skinnedmeshrenderer.SetBlendShapeWeight(46, 0); //UpperLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(47, 0); //UpperLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(48, 0); //UpperLipUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(49, 0); //UpperLipUp_Right

        subs.text = "Eu dou-me bem com os meus pais";
        yield return new WaitForSeconds(2.0f);
        subs.text = "";
        ///
        yield return new WaitForSeconds(3.0f);
        skinnedmeshrenderer.SetBlendShapeWeight(0, 0); //Blink_Left
        skinnedmeshrenderer.SetBlendShapeWeight(1, 0); //Blink_Right
        skinnedmeshrenderer.SetBlendShapeWeight(2, 20); //BrowsDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(3, 20); //BrowsDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(4, 0); //BrowsIn_Left
        skinnedmeshrenderer.SetBlendShapeWeight(5, 0); //BrowsIn_Right
        skinnedmeshrenderer.SetBlendShapeWeight(6, 100); //BrowsOuterLower_Left
        skinnedmeshrenderer.SetBlendShapeWeight(7, 100); //BrowsOuterLower_Right
        skinnedmeshrenderer.SetBlendShapeWeight(8, 0); //BrowsUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(9, 0); //BrowsUp_Right
        skinnedmeshrenderer.SetBlendShapeWeight(10, 0); //CheekPuff_Left
        skinnedmeshrenderer.SetBlendShapeWeight(11, 0); //CheekPuff_Right
        skinnedmeshrenderer.SetBlendShapeWeight(12, 0); //EyeWide_Left
        skinnedmeshrenderer.SetBlendShapeWeight(13, 0); //EyeWide_Right
        skinnedmeshrenderer.SetBlendShapeWeight(14, 0); //Frown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(15, 0); //Frown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(16, 0); //JawBackward
        skinnedmeshrenderer.SetBlendShapeWeight(17, 0); //JawForeward
        skinnedmeshrenderer.SetBlendShapeWeight(18, 0); //JawRotateY_Left
        skinnedmeshrenderer.SetBlendShapeWeight(19, 0); //JawRotateY_Right
        skinnedmeshrenderer.SetBlendShapeWeight(20, 0); //JawRotateZ_Left
        skinnedmeshrenderer.SetBlendShapeWeight(21, 0); //JawRotateZ_Right
        skinnedmeshrenderer.SetBlendShapeWeight(22, 0); //Jaw_Down
        skinnedmeshrenderer.SetBlendShapeWeight(23, 0); //Jaw_Left
        skinnedmeshrenderer.SetBlendShapeWeight(24, 0); //Jaw_Right
        skinnedmeshrenderer.SetBlendShapeWeight(25, 100); //Jaw_Up
        skinnedmeshrenderer.SetBlendShapeWeight(26, 0); //LowerLipDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(27, 0); //LowerLipDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(28, 0); //LowerLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(29, 0); //LowerLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(30, 0); //Midmouth_Left
        skinnedmeshrenderer.SetBlendShapeWeight(31, 0); //Midmouth_Right
        skinnedmeshrenderer.SetBlendShapeWeight(32, 0); //MouthDown
        skinnedmeshrenderer.SetBlendShapeWeight(33, 0); //MouthNarrow_Left
        skinnedmeshrenderer.SetBlendShapeWeight(34, 0); //MouthNarrow_Right
        skinnedmeshrenderer.SetBlendShapeWeight(35, 0); //MouthOpen
        skinnedmeshrenderer.SetBlendShapeWeight(36, 0); //MouthUp
        skinnedmeshrenderer.SetBlendShapeWeight(37, 0); //MouthWhistle_Narrow_Adjust_Left
        skinnedmeshrenderer.SetBlendShapeWeight(38, 0); //MouthWhitstle_Narrow_Adjust_Right
        skinnedmeshrenderer.SetBlendShapeWeight(39, 0); //NoseScrunch_Left
        skinnedmeshrenderer.SetBlendShapeWeight(40, 0); //NoseScrunch_Right
        skinnedmeshrenderer.SetBlendShapeWeight(41, 0); //Smile_Left
        skinnedmeshrenderer.SetBlendShapeWeight(42, 0); //Smile_Right
        skinnedmeshrenderer.SetBlendShapeWeight(43, 50); //Squint_Left
        skinnedmeshrenderer.SetBlendShapeWeight(44, 50); //Squint_Right
        skinnedmeshrenderer.SetBlendShapeWeight(45, 0); //TongueUp
        skinnedmeshrenderer.SetBlendShapeWeight(46, 0); //UpperLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(47, 0); //UpperLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(48, 0); //UpperLipUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(49, 0); //UpperLipUp_Right

        subs.text = "Eles são compreensivos comigo";
        yield return new WaitForSeconds(2.0f);
        subs.text = "";
        ////
        yield return new WaitForSeconds(1.5f);
        skinnedmeshrenderer.SetBlendShapeWeight(0, 0); //Blink_Left
        skinnedmeshrenderer.SetBlendShapeWeight(1, 0); //Blink_Right
        skinnedmeshrenderer.SetBlendShapeWeight(2, 0); //BrowsDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(3, 0); //BrowsDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(4, 0); //BrowsIn_Left
        skinnedmeshrenderer.SetBlendShapeWeight(5, 0); //BrowsIn_Right
        skinnedmeshrenderer.SetBlendShapeWeight(6, 100); //BrowsOuterLower_Left
        skinnedmeshrenderer.SetBlendShapeWeight(7, 100); //BrowsOuterLower_Right
        skinnedmeshrenderer.SetBlendShapeWeight(8, 0); //BrowsUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(9, 0); //BrowsUp_Right
        skinnedmeshrenderer.SetBlendShapeWeight(10, 0); //CheekPuff_Left
        skinnedmeshrenderer.SetBlendShapeWeight(11, 0); //CheekPuff_Right
        skinnedmeshrenderer.SetBlendShapeWeight(12, 0); //EyeWide_Left
        skinnedmeshrenderer.SetBlendShapeWeight(13, 0); //EyeWide_Right
        skinnedmeshrenderer.SetBlendShapeWeight(14, 0); //Frown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(15, 0); //Frown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(16, 0); //JawBackward
        skinnedmeshrenderer.SetBlendShapeWeight(17, 0); //JawForeward
        skinnedmeshrenderer.SetBlendShapeWeight(18, 0); //JawRotateY_Left
        skinnedmeshrenderer.SetBlendShapeWeight(19, 0); //JawRotateY_Right
        skinnedmeshrenderer.SetBlendShapeWeight(20, 0); //JawRotateZ_Left
        skinnedmeshrenderer.SetBlendShapeWeight(21, 0); //JawRotateZ_Right
        skinnedmeshrenderer.SetBlendShapeWeight(22, 0); //Jaw_Down
        skinnedmeshrenderer.SetBlendShapeWeight(23, 0); //Jaw_Left
        skinnedmeshrenderer.SetBlendShapeWeight(24, 0); //Jaw_Right
        skinnedmeshrenderer.SetBlendShapeWeight(25, 0); //Jaw_Up
        skinnedmeshrenderer.SetBlendShapeWeight(26, 0); //LowerLipDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(27, 0); //LowerLipDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(28, 0); //LowerLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(29, 0); //LowerLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(30, 0); //Midmouth_Left
        skinnedmeshrenderer.SetBlendShapeWeight(31, 0); //Midmouth_Right
        skinnedmeshrenderer.SetBlendShapeWeight(32, 0); //MouthDown
        skinnedmeshrenderer.SetBlendShapeWeight(33, 0); //MouthNarrow_Left
        skinnedmeshrenderer.SetBlendShapeWeight(34, 0); //MouthNarrow_Right
        skinnedmeshrenderer.SetBlendShapeWeight(35, 0); //MouthOpen
        skinnedmeshrenderer.SetBlendShapeWeight(36, 25); //MouthUp
        skinnedmeshrenderer.SetBlendShapeWeight(37, 0); //MouthWhistle_Narrow_Adjust_Left
        skinnedmeshrenderer.SetBlendShapeWeight(38, 0); //MouthWhitstle_Narrow_Adjust_Right
        skinnedmeshrenderer.SetBlendShapeWeight(39, 0); //NoseScrunch_Left
        skinnedmeshrenderer.SetBlendShapeWeight(40, 0); //NoseScrunch_Right
        skinnedmeshrenderer.SetBlendShapeWeight(41, 20); //Smile_Left
        skinnedmeshrenderer.SetBlendShapeWeight(42, 20); //Smile_Right
        skinnedmeshrenderer.SetBlendShapeWeight(43, 50); //Squint_Left
        skinnedmeshrenderer.SetBlendShapeWeight(44, 50); //Squint_Right
        skinnedmeshrenderer.SetBlendShapeWeight(45, 0); //TongueUp
        skinnedmeshrenderer.SetBlendShapeWeight(46, 0); //UpperLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(47, 0); //UpperLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(48, 0); //UpperLipUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(49, 0); //UpperLipUp_Right

        subs.text = "Eles gostam de mim";
        yield return new WaitForSeconds(2.0f);
        subs.text = "";
        ///
        yield return new WaitForSeconds(2.0f);
        skinnedmeshrenderer.SetBlendShapeWeight(0, 0); //Blink_Left
        skinnedmeshrenderer.SetBlendShapeWeight(1, 0); //Blink_Right
        skinnedmeshrenderer.SetBlendShapeWeight(2, 0); //BrowsDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(3, 0); //BrowsDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(4, 0); //BrowsIn_Left
        skinnedmeshrenderer.SetBlendShapeWeight(5, 0); //BrowsIn_Right
        skinnedmeshrenderer.SetBlendShapeWeight(6, 100); //BrowsOuterLower_Left
        skinnedmeshrenderer.SetBlendShapeWeight(7, 100); //BrowsOuterLower_Right
        skinnedmeshrenderer.SetBlendShapeWeight(8, 0); //BrowsUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(9, 0); //BrowsUp_Right
        skinnedmeshrenderer.SetBlendShapeWeight(10, 0); //CheekPuff_Left
        skinnedmeshrenderer.SetBlendShapeWeight(11, 0); //CheekPuff_Right
        skinnedmeshrenderer.SetBlendShapeWeight(12, 0); //EyeWide_Left
        skinnedmeshrenderer.SetBlendShapeWeight(13, 0); //EyeWide_Right
        skinnedmeshrenderer.SetBlendShapeWeight(14, 0); //Frown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(15, 0); //Frown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(16, 0); //JawBackward
        skinnedmeshrenderer.SetBlendShapeWeight(17, 0); //JawForeward
        skinnedmeshrenderer.SetBlendShapeWeight(18, 0); //JawRotateY_Left
        skinnedmeshrenderer.SetBlendShapeWeight(19, 0); //JawRotateY_Right
        skinnedmeshrenderer.SetBlendShapeWeight(20, 0); //JawRotateZ_Left
        skinnedmeshrenderer.SetBlendShapeWeight(21, 0); //JawRotateZ_Right
        skinnedmeshrenderer.SetBlendShapeWeight(22, 0); //Jaw_Down
        skinnedmeshrenderer.SetBlendShapeWeight(23, 0); //Jaw_Left
        skinnedmeshrenderer.SetBlendShapeWeight(24, 0); //Jaw_Right
        skinnedmeshrenderer.SetBlendShapeWeight(25, 100); //Jaw_Up
        skinnedmeshrenderer.SetBlendShapeWeight(26, 0); //LowerLipDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(27, 0); //LowerLipDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(28, 0); //LowerLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(29, 0); //LowerLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(30, 0); //Midmouth_Left
        skinnedmeshrenderer.SetBlendShapeWeight(31, 0); //Midmouth_Right
        skinnedmeshrenderer.SetBlendShapeWeight(32, 25); //MouthDown
        skinnedmeshrenderer.SetBlendShapeWeight(33, 0); //MouthNarrow_Left
        skinnedmeshrenderer.SetBlendShapeWeight(34, 0); //MouthNarrow_Right
        skinnedmeshrenderer.SetBlendShapeWeight(35, 0); //MouthOpen
        skinnedmeshrenderer.SetBlendShapeWeight(36, 25); //MouthUp
        skinnedmeshrenderer.SetBlendShapeWeight(37, 0); //MouthWhistle_Narrow_Adjust_Left
        skinnedmeshrenderer.SetBlendShapeWeight(38, 0); //MouthWhitstle_Narrow_Adjust_Right
        skinnedmeshrenderer.SetBlendShapeWeight(39, 0); //NoseScrunch_Left
        skinnedmeshrenderer.SetBlendShapeWeight(40, 0); //NoseScrunch_Right
        skinnedmeshrenderer.SetBlendShapeWeight(41, 0); //Smile_Left
        skinnedmeshrenderer.SetBlendShapeWeight(42, 0); //Smile_Right
        skinnedmeshrenderer.SetBlendShapeWeight(43, 50); //Squint_Left
        skinnedmeshrenderer.SetBlendShapeWeight(44, 50); //Squint_Right
        skinnedmeshrenderer.SetBlendShapeWeight(45, 0); //TongueUp
        skinnedmeshrenderer.SetBlendShapeWeight(46, 0); //UpperLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(47, 0); //UpperLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(48, 0); //UpperLipUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(49, 0); //UpperLipUp_Right
        //

        subs.text = "Mas eu não sei como lhes dizer";
        yield return new WaitForSeconds(2.0f);
        subs.text = "";
        ///
        yield return new WaitForSeconds(2.0f);
        skinnedmeshrenderer.SetBlendShapeWeight(0, 0); //Blink_Left
        skinnedmeshrenderer.SetBlendShapeWeight(1, 0); //Blink_Right
        skinnedmeshrenderer.SetBlendShapeWeight(2, 0); //BrowsDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(3, 0); //BrowsDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(4, 0); //BrowsIn_Left
        skinnedmeshrenderer.SetBlendShapeWeight(5, 0); //BrowsIn_Right
        skinnedmeshrenderer.SetBlendShapeWeight(6, 100); //BrowsOuterLower_Left
        skinnedmeshrenderer.SetBlendShapeWeight(7, 100); //BrowsOuterLower_Right
        skinnedmeshrenderer.SetBlendShapeWeight(8, 0); //BrowsUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(9, 0); //BrowsUp_Right
        skinnedmeshrenderer.SetBlendShapeWeight(10, 0); //CheekPuff_Left
        skinnedmeshrenderer.SetBlendShapeWeight(11, 0); //CheekPuff_Right
        skinnedmeshrenderer.SetBlendShapeWeight(12, 0); //EyeWide_Left
        skinnedmeshrenderer.SetBlendShapeWeight(13, 0); //EyeWide_Right
        skinnedmeshrenderer.SetBlendShapeWeight(14, 0); //Frown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(15, 0); //Frown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(16, 0); //JawBackward
        skinnedmeshrenderer.SetBlendShapeWeight(17, 0); //JawForeward
        skinnedmeshrenderer.SetBlendShapeWeight(18, 0); //JawRotateY_Left
        skinnedmeshrenderer.SetBlendShapeWeight(19, 0); //JawRotateY_Right
        skinnedmeshrenderer.SetBlendShapeWeight(20, 0); //JawRotateZ_Left
        skinnedmeshrenderer.SetBlendShapeWeight(21, 0); //JawRotateZ_Right
        skinnedmeshrenderer.SetBlendShapeWeight(22, 0); //Jaw_Down
        skinnedmeshrenderer.SetBlendShapeWeight(23, 0); //Jaw_Left
        skinnedmeshrenderer.SetBlendShapeWeight(24, 0); //Jaw_Right
        skinnedmeshrenderer.SetBlendShapeWeight(25, 100); //Jaw_Up
        skinnedmeshrenderer.SetBlendShapeWeight(26, 0); //LowerLipDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(27, 0); //LowerLipDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(28, 0); //LowerLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(29, 0); //LowerLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(30, 0); //Midmouth_Left
        skinnedmeshrenderer.SetBlendShapeWeight(31, 0); //Midmouth_Right
        skinnedmeshrenderer.SetBlendShapeWeight(32, 25); //MouthDown
        skinnedmeshrenderer.SetBlendShapeWeight(33, 0); //MouthNarrow_Left
        skinnedmeshrenderer.SetBlendShapeWeight(34, 0); //MouthNarrow_Right
        skinnedmeshrenderer.SetBlendShapeWeight(35, 0); //MouthOpen
        skinnedmeshrenderer.SetBlendShapeWeight(36, 25); //MouthUp
        skinnedmeshrenderer.SetBlendShapeWeight(37, 0); //MouthWhistle_Narrow_Adjust_Left
        skinnedmeshrenderer.SetBlendShapeWeight(38, 0); //MouthWhitstle_Narrow_Adjust_Right
        skinnedmeshrenderer.SetBlendShapeWeight(39, 0); //NoseScrunch_Left
        skinnedmeshrenderer.SetBlendShapeWeight(40, 0); //NoseScrunch_Right
        skinnedmeshrenderer.SetBlendShapeWeight(41, 0); //Smile_Left
        skinnedmeshrenderer.SetBlendShapeWeight(42, 0); //Smile_Right
        skinnedmeshrenderer.SetBlendShapeWeight(43, 50); //Squint_Left
        skinnedmeshrenderer.SetBlendShapeWeight(44, 50); //Squint_Right
        skinnedmeshrenderer.SetBlendShapeWeight(45, 0); //TongueUp
        skinnedmeshrenderer.SetBlendShapeWeight(46, 0); //UpperLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(47, 0); //UpperLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(48, 0); //UpperLipUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(49, 0); //UpperLipUp_Right
        //
        subs.text = "O que é que podia acontecer, o que é que eles iam achar";
        yield return new WaitForSeconds(2.5f);
        subs.text = "";
        ////
        skinnedmeshrenderer.SetBlendShapeWeight(0, 0); //Blink_Left
        skinnedmeshrenderer.SetBlendShapeWeight(1, 0); //Blink_Right
        skinnedmeshrenderer.SetBlendShapeWeight(2, 0); //BrowsDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(3, 0); //BrowsDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(4, 0); //BrowsIn_Left
        skinnedmeshrenderer.SetBlendShapeWeight(5, 0); //BrowsIn_Right
        skinnedmeshrenderer.SetBlendShapeWeight(6, 100); //BrowsOuterLower_Left
        skinnedmeshrenderer.SetBlendShapeWeight(7, 100); //BrowsOuterLower_Right
        skinnedmeshrenderer.SetBlendShapeWeight(8, 0); //BrowsUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(9, 0); //BrowsUp_Right
        skinnedmeshrenderer.SetBlendShapeWeight(10, 0); //CheekPuff_Left
        skinnedmeshrenderer.SetBlendShapeWeight(11, 0); //CheekPuff_Right
        skinnedmeshrenderer.SetBlendShapeWeight(12, 0); //EyeWide_Left
        skinnedmeshrenderer.SetBlendShapeWeight(13, 0); //EyeWide_Right
        skinnedmeshrenderer.SetBlendShapeWeight(14, 0); //Frown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(15, 0); //Frown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(16, 0); //JawBackward
        skinnedmeshrenderer.SetBlendShapeWeight(17, 0); //JawForeward
        skinnedmeshrenderer.SetBlendShapeWeight(18, 0); //JawRotateY_Left
        skinnedmeshrenderer.SetBlendShapeWeight(19, 0); //JawRotateY_Right
        skinnedmeshrenderer.SetBlendShapeWeight(20, 0); //JawRotateZ_Left
        skinnedmeshrenderer.SetBlendShapeWeight(21, 0); //JawRotateZ_Right
        skinnedmeshrenderer.SetBlendShapeWeight(22, 0); //Jaw_Down
        skinnedmeshrenderer.SetBlendShapeWeight(23, 0); //Jaw_Left
        skinnedmeshrenderer.SetBlendShapeWeight(24, 0); //Jaw_Right
        skinnedmeshrenderer.SetBlendShapeWeight(25, 0); //Jaw_Up
        skinnedmeshrenderer.SetBlendShapeWeight(26, 0); //LowerLipDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(27, 0); //LowerLipDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(28, 0); //LowerLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(29, 0); //LowerLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(30, 0); //Midmouth_Left
        skinnedmeshrenderer.SetBlendShapeWeight(31, 0); //Midmouth_Right
        skinnedmeshrenderer.SetBlendShapeWeight(32, 25); //MouthDown
        skinnedmeshrenderer.SetBlendShapeWeight(33, 0); //MouthNarrow_Left
        skinnedmeshrenderer.SetBlendShapeWeight(34, 0); //MouthNarrow_Right
        skinnedmeshrenderer.SetBlendShapeWeight(35, 0); //MouthOpen
        skinnedmeshrenderer.SetBlendShapeWeight(36, 25); //MouthUp
        skinnedmeshrenderer.SetBlendShapeWeight(37, 0); //MouthWhistle_Narrow_Adjust_Left
        skinnedmeshrenderer.SetBlendShapeWeight(38, 0); //MouthWhitstle_Narrow_Adjust_Right
        skinnedmeshrenderer.SetBlendShapeWeight(39, 0); //NoseScrunch_Left
        skinnedmeshrenderer.SetBlendShapeWeight(40, 0); //NoseScrunch_Right
        skinnedmeshrenderer.SetBlendShapeWeight(41, 0); //Smile_Left
        skinnedmeshrenderer.SetBlendShapeWeight(42, 0); //Smile_Right
        skinnedmeshrenderer.SetBlendShapeWeight(43, 70); //Squint_Left
        skinnedmeshrenderer.SetBlendShapeWeight(44, 70); //Squint_Right
        skinnedmeshrenderer.SetBlendShapeWeight(45, 0); //TongueUp
        skinnedmeshrenderer.SetBlendShapeWeight(46, 0); //UpperLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(47, 0); //UpperLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(48, 0); //UpperLipUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(49, 0); //UpperLipUp_Right

        yield return new WaitForSeconds(2.0f);
        skinnedmeshrenderer.SetBlendShapeWeight(0, 0); //Blink_Left
        skinnedmeshrenderer.SetBlendShapeWeight(1, 0); //Blink_Right
        skinnedmeshrenderer.SetBlendShapeWeight(2, 0); //BrowsDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(3, 0); //BrowsDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(4, 0); //BrowsIn_Left
        skinnedmeshrenderer.SetBlendShapeWeight(5, 0); //BrowsIn_Right
        skinnedmeshrenderer.SetBlendShapeWeight(6, 100); //BrowsOuterLower_Left
        skinnedmeshrenderer.SetBlendShapeWeight(7, 100); //BrowsOuterLower_Right
        skinnedmeshrenderer.SetBlendShapeWeight(8, 0); //BrowsUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(9, 0); //BrowsUp_Right
        skinnedmeshrenderer.SetBlendShapeWeight(10, 0); //CheekPuff_Left
        skinnedmeshrenderer.SetBlendShapeWeight(11, 0); //CheekPuff_Right
        skinnedmeshrenderer.SetBlendShapeWeight(12, 0); //EyeWide_Left
        skinnedmeshrenderer.SetBlendShapeWeight(13, 0); //EyeWide_Right
        skinnedmeshrenderer.SetBlendShapeWeight(14, 0); //Frown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(15, 0); //Frown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(16, 0); //JawBackward
        skinnedmeshrenderer.SetBlendShapeWeight(17, 0); //JawForeward
        skinnedmeshrenderer.SetBlendShapeWeight(18, 0); //JawRotateY_Left
        skinnedmeshrenderer.SetBlendShapeWeight(19, 0); //JawRotateY_Right
        skinnedmeshrenderer.SetBlendShapeWeight(20, 0); //JawRotateZ_Left
        skinnedmeshrenderer.SetBlendShapeWeight(21, 0); //JawRotateZ_Right
        skinnedmeshrenderer.SetBlendShapeWeight(22, 0); //Jaw_Down
        skinnedmeshrenderer.SetBlendShapeWeight(23, 0); //Jaw_Left
        skinnedmeshrenderer.SetBlendShapeWeight(24, 0); //Jaw_Right
        skinnedmeshrenderer.SetBlendShapeWeight(25, 0); //Jaw_Up
        skinnedmeshrenderer.SetBlendShapeWeight(26, 0); //LowerLipDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(27, 0); //LowerLipDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(28, 0); //LowerLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(29, 0); //LowerLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(30, 0); //Midmouth_Left
        skinnedmeshrenderer.SetBlendShapeWeight(31, 0); //Midmouth_Right
        skinnedmeshrenderer.SetBlendShapeWeight(32, 25); //MouthDown
        skinnedmeshrenderer.SetBlendShapeWeight(33, 0); //MouthNarrow_Left
        skinnedmeshrenderer.SetBlendShapeWeight(34, 0); //MouthNarrow_Right
        skinnedmeshrenderer.SetBlendShapeWeight(35, 0); //MouthOpen
        skinnedmeshrenderer.SetBlendShapeWeight(36, 25); //MouthUp
        skinnedmeshrenderer.SetBlendShapeWeight(37, 0); //MouthWhistle_Narrow_Adjust_Left
        skinnedmeshrenderer.SetBlendShapeWeight(38, 0); //MouthWhitstle_Narrow_Adjust_Right
        skinnedmeshrenderer.SetBlendShapeWeight(39, 0); //NoseScrunch_Left
        skinnedmeshrenderer.SetBlendShapeWeight(40, 0); //NoseScrunch_Right
        skinnedmeshrenderer.SetBlendShapeWeight(41, 0); //Smile_Left
        skinnedmeshrenderer.SetBlendShapeWeight(42, 0); //Smile_Right
        skinnedmeshrenderer.SetBlendShapeWeight(43, 100); //Squint_Left
        skinnedmeshrenderer.SetBlendShapeWeight(44, 100); //Squint_Right
        skinnedmeshrenderer.SetBlendShapeWeight(45, 0); //TongueUp
        skinnedmeshrenderer.SetBlendShapeWeight(46, 0); //UpperLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(47, 0); //UpperLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(48, 0); //UpperLipUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(49, 0); //UpperLipUp_Right

        subs.text = "Eles podiam achar que eu é que sou culpada";
        yield return new WaitForSeconds(4.0f);
        subs.text = "";
        ////
        skinnedmeshrenderer.SetBlendShapeWeight(0, 0); //Blink_Left
        skinnedmeshrenderer.SetBlendShapeWeight(1, 0); //Blink_Right
        skinnedmeshrenderer.SetBlendShapeWeight(2, 0); //BrowsDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(3, 0); //BrowsDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(4, 0); //BrowsIn_Left
        skinnedmeshrenderer.SetBlendShapeWeight(5, 0); //BrowsIn_Right
        skinnedmeshrenderer.SetBlendShapeWeight(6, 100); //BrowsOuterLower_Left
        skinnedmeshrenderer.SetBlendShapeWeight(7, 100); //BrowsOuterLower_Right
        skinnedmeshrenderer.SetBlendShapeWeight(8, 0); //BrowsUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(9, 0); //BrowsUp_Right
        skinnedmeshrenderer.SetBlendShapeWeight(10, 0); //CheekPuff_Left
        skinnedmeshrenderer.SetBlendShapeWeight(11, 0); //CheekPuff_Right
        skinnedmeshrenderer.SetBlendShapeWeight(12, 0); //EyeWide_Left
        skinnedmeshrenderer.SetBlendShapeWeight(13, 0); //EyeWide_Right
        skinnedmeshrenderer.SetBlendShapeWeight(14, 0); //Frown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(15, 0); //Frown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(16, 0); //JawBackward
        skinnedmeshrenderer.SetBlendShapeWeight(17, 0); //JawForeward
        skinnedmeshrenderer.SetBlendShapeWeight(18, 0); //JawRotateY_Left
        skinnedmeshrenderer.SetBlendShapeWeight(19, 0); //JawRotateY_Right
        skinnedmeshrenderer.SetBlendShapeWeight(20, 0); //JawRotateZ_Left
        skinnedmeshrenderer.SetBlendShapeWeight(21, 0); //JawRotateZ_Right
        skinnedmeshrenderer.SetBlendShapeWeight(22, 0); //Jaw_Down
        skinnedmeshrenderer.SetBlendShapeWeight(23, 0); //Jaw_Left
        skinnedmeshrenderer.SetBlendShapeWeight(24, 0); //Jaw_Right
        skinnedmeshrenderer.SetBlendShapeWeight(25, 0); //Jaw_Up
        skinnedmeshrenderer.SetBlendShapeWeight(26, 0); //LowerLipDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(27, 0); //LowerLipDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(28, 0); //LowerLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(29, 0); //LowerLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(30, 0); //Midmouth_Left
        skinnedmeshrenderer.SetBlendShapeWeight(31, 0); //Midmouth_Right
        skinnedmeshrenderer.SetBlendShapeWeight(32, 25); //MouthDown
        skinnedmeshrenderer.SetBlendShapeWeight(33, 0); //MouthNarrow_Left
        skinnedmeshrenderer.SetBlendShapeWeight(34, 0); //MouthNarrow_Right
        skinnedmeshrenderer.SetBlendShapeWeight(35, 0); //MouthOpen
        skinnedmeshrenderer.SetBlendShapeWeight(36, 25); //MouthUp
        skinnedmeshrenderer.SetBlendShapeWeight(37, 0); //MouthWhistle_Narrow_Adjust_Left
        skinnedmeshrenderer.SetBlendShapeWeight(38, 0); //MouthWhitstle_Narrow_Adjust_Right
        skinnedmeshrenderer.SetBlendShapeWeight(39, 0); //NoseScrunch_Left
        skinnedmeshrenderer.SetBlendShapeWeight(40, 0); //NoseScrunch_Right
        skinnedmeshrenderer.SetBlendShapeWeight(41, 0); //Smile_Left
        skinnedmeshrenderer.SetBlendShapeWeight(42, 0); //Smile_Right
        skinnedmeshrenderer.SetBlendShapeWeight(43, 100); //Squint_Left
        skinnedmeshrenderer.SetBlendShapeWeight(44, 100); //Squint_Right
        skinnedmeshrenderer.SetBlendShapeWeight(45, 0); //TongueUp
        skinnedmeshrenderer.SetBlendShapeWeight(46, 0); //UpperLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(47, 0); //UpperLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(48, 0); //UpperLipUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(49, 0); //UpperLipUp_Right
        //
        yield return new WaitForSeconds(2.0f);
        skinnedmeshrenderer.SetBlendShapeWeight(0, 0); //Blink_Left
        skinnedmeshrenderer.SetBlendShapeWeight(1, 0); //BlinkRight
        skinnedmeshrenderer.SetBlendShapeWeight(2, 0); //BrowsDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(3, 0); //BrowsDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(4, 0); //BrowsIn_Left
        skinnedmeshrenderer.SetBlendShapeWeight(5, 0); //BrowsIn_Right
        skinnedmeshrenderer.SetBlendShapeWeight(6, 100); //BrowsOuterLower_Left
        skinnedmeshrenderer.SetBlendShapeWeight(7, 100); //BrowsOuterLower_RIght
        skinnedmeshrenderer.SetBlendShapeWeight(8, 0); //BrowsUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(9, 0); //BrowsUp_Right
        skinnedmeshrenderer.SetBlendShapeWeight(10, 0); //CheekPuff_Left
        skinnedmeshrenderer.SetBlendShapeWeight(11, 0); //CheekPuff_RIght
        skinnedmeshrenderer.SetBlendShapeWeight(12, 0); //EyeWide_Left
        skinnedmeshrenderer.SetBlendShapeWeight(13, 0); //EyeWide_Right
        skinnedmeshrenderer.SetBlendShapeWeight(14, 0); //Frown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(15, 0); //Frown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(16, 0); //JawBackward
        skinnedmeshrenderer.SetBlendShapeWeight(17, 0); //JawForeward
        skinnedmeshrenderer.SetBlendShapeWeight(18, 0); //JawRotateY_Left
        skinnedmeshrenderer.SetBlendShapeWeight(19, 0); //JawRotateY_Right
        skinnedmeshrenderer.SetBlendShapeWeight(20, 0); //JawRotateZ_Left
        skinnedmeshrenderer.SetBlendShapeWeight(21, 0); //JawRotateZ_Right
        skinnedmeshrenderer.SetBlendShapeWeight(22, 0); //Jaw_Down
        skinnedmeshrenderer.SetBlendShapeWeight(23, 0); //Jaw_Left
        skinnedmeshrenderer.SetBlendShapeWeight(24, 0); //Jaw_Right
        skinnedmeshrenderer.SetBlendShapeWeight(25, 0); //Jaw_Up
        skinnedmeshrenderer.SetBlendShapeWeight(26, 0); //LowerLipDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(27, 0); //LowerLipDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(28, 0); //LowerLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(29, 0); //LowerLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(30, 0); //Midmouth_Left
        skinnedmeshrenderer.SetBlendShapeWeight(31, 0); //Midmouth_Right
        skinnedmeshrenderer.SetBlendShapeWeight(32, 25); //MouthDown
        skinnedmeshrenderer.SetBlendShapeWeight(33, 0); //MouthNarrow_Left
        skinnedmeshrenderer.SetBlendShapeWeight(34, 0); //MouthNarrow_Right
        skinnedmeshrenderer.SetBlendShapeWeight(35, 0); //MouthOpen
        skinnedmeshrenderer.SetBlendShapeWeight(36, 25); //MouthUp
        skinnedmeshrenderer.SetBlendShapeWeight(37, 0); //MouthWhistle_NarrowAdjust_Left
        skinnedmeshrenderer.SetBlendShapeWeight(38, 0); //MouthWhistle_NarrowAdjust_Right
        skinnedmeshrenderer.SetBlendShapeWeight(39, 0); //NoseScrunch_Left
        skinnedmeshrenderer.SetBlendShapeWeight(40, 0); //NoseScrunch_Right
        skinnedmeshrenderer.SetBlendShapeWeight(41, 0); //Smile_Left
        skinnedmeshrenderer.SetBlendShapeWeight(42, 0); //Smile_Right
        skinnedmeshrenderer.SetBlendShapeWeight(43, 100); //Squint_Left
        skinnedmeshrenderer.SetBlendShapeWeight(44, 100); //Squint_Right
        skinnedmeshrenderer.SetBlendShapeWeight(45, 0); //TongueUp
        skinnedmeshrenderer.SetBlendShapeWeight(46, 0); //UpperLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(47, 0); //UpperLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(48, 0); //UpperLipUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(49, 0); //UpperLipUp_Right
        subs.text = "E a minha irmã?";
        yield return new WaitForSeconds(2.0f);
        subs.text = "";
        ////
        yield return new WaitForSeconds(1.5f);
        skinnedmeshrenderer.SetBlendShapeWeight(0, 0); //Blink_Left
        skinnedmeshrenderer.SetBlendShapeWeight(1, 0); //Blink_Right
        skinnedmeshrenderer.SetBlendShapeWeight(2, 50); //BrowsDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(3, 50); //BrowsDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(4, 100); //BrowsIn_Left
        skinnedmeshrenderer.SetBlendShapeWeight(5, 100); //BrowsIn_Right
        skinnedmeshrenderer.SetBlendShapeWeight(6, 100); //BrowsOuterLower_Left
        skinnedmeshrenderer.SetBlendShapeWeight(7, 100); //BrowsOuterLower_Right
        skinnedmeshrenderer.SetBlendShapeWeight(8, 0); //BrowsUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(9, 0); //BrowsUp_Right
        skinnedmeshrenderer.SetBlendShapeWeight(10, 0); //CheekPuff_Left
        skinnedmeshrenderer.SetBlendShapeWeight(11, 0); //CheekPuff_Right
        skinnedmeshrenderer.SetBlendShapeWeight(12, 0); //EyeWide_Left
        skinnedmeshrenderer.SetBlendShapeWeight(13, 0); //EyeWide_Right
        skinnedmeshrenderer.SetBlendShapeWeight(14, 0); //Frown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(15, 0); //Frown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(16, 0); //JawBackward
        skinnedmeshrenderer.SetBlendShapeWeight(17, 0); //JawForeward
        skinnedmeshrenderer.SetBlendShapeWeight(18, 0); //JawRotateY_Left
        skinnedmeshrenderer.SetBlendShapeWeight(19, 0); //JawRotateY_Right
        skinnedmeshrenderer.SetBlendShapeWeight(20, 0); //JawRotateZ_Left
        skinnedmeshrenderer.SetBlendShapeWeight(21, 0); //JawRotateZ_Right
        skinnedmeshrenderer.SetBlendShapeWeight(22, 0); //Jaw_Down
        skinnedmeshrenderer.SetBlendShapeWeight(23, 0); //Jaw_Left
        skinnedmeshrenderer.SetBlendShapeWeight(24, 0); //Jaw_Right
        skinnedmeshrenderer.SetBlendShapeWeight(25, 0); //Jaw_Up
        skinnedmeshrenderer.SetBlendShapeWeight(26, 0); //LowerLipDown_Left
        skinnedmeshrenderer.SetBlendShapeWeight(27, 0); //LowerLipDown_Right
        skinnedmeshrenderer.SetBlendShapeWeight(28, 0); //LowerLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(29, 0); //LowerLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(30, 0); //Midmouth_Left
        skinnedmeshrenderer.SetBlendShapeWeight(31, 0); //Midmouth_Right
        skinnedmeshrenderer.SetBlendShapeWeight(32, 0); //MouthDown
        skinnedmeshrenderer.SetBlendShapeWeight(33, 0); //MouthNarrow_Left
        skinnedmeshrenderer.SetBlendShapeWeight(34, 0); //MouthNarrow_Right
        skinnedmeshrenderer.SetBlendShapeWeight(35, 0); //MouthOpen
        skinnedmeshrenderer.SetBlendShapeWeight(36, 0); //MouthUp
        skinnedmeshrenderer.SetBlendShapeWeight(37, 0); //MouthWhistle_Narrow_Adjust_Left
        skinnedmeshrenderer.SetBlendShapeWeight(38, 0); //MouthWhitstle_Narrow_Adjust_Right
        skinnedmeshrenderer.SetBlendShapeWeight(39, 0); //NoseScrunch_Left
        skinnedmeshrenderer.SetBlendShapeWeight(40, 0); //NoseScrunch_Right
        skinnedmeshrenderer.SetBlendShapeWeight(41, 0); //Smile_Left
        skinnedmeshrenderer.SetBlendShapeWeight(42, 0); //Smile_Right
        skinnedmeshrenderer.SetBlendShapeWeight(43, 100); //Squint_Left
        skinnedmeshrenderer.SetBlendShapeWeight(44, 100); //Squint_Right
        skinnedmeshrenderer.SetBlendShapeWeight(45, 0); //TongueUp
        skinnedmeshrenderer.SetBlendShapeWeight(46, 0); //UpperLipIn
        skinnedmeshrenderer.SetBlendShapeWeight(47, 0); //UpperLipOut
        skinnedmeshrenderer.SetBlendShapeWeight(48, 0); //UpperLipUp_Left
        skinnedmeshrenderer.SetBlendShapeWeight(49, 0); //UpperLipUp_Right
        subs.text = "O que é que ela vai sentir?";
        yield return new WaitForSeconds(2.0f);
        subs.text = "";
        /////
        yield return new WaitForSeconds(2.0f);
        subs.text = "O que é que podia acontecer?";
        yield return new WaitForSeconds(2.0f);
        subs.text = "";
        /////
        yield return new WaitForSeconds(4.0f);
        subs.text = "Não lhes posso contar";
        yield return new WaitForSeconds(2.0f);
        subs.text = "";
        /////
    }*/

    private IEnumerator Info1_B() {

        yield return new WaitForSeconds(3.5f);

        subs.text = "Eu dou-me bem com os meus pais, eles são compreensivos comigo, eles gostam de mim.";
        yield return new WaitForSeconds(10.0f);
        subs.text = "";
        ///
        yield return new WaitForSeconds(3.0f);
        subs.text = "Mas eu não sei como lhes dizer. O que é que podia acontecer?";
        yield return new WaitForSeconds(5.0f);
        subs.text = "";
        ///
        yield return new WaitForSeconds(0.0f);
        subs.text = "O que é que eles iam achar?";
        yield return new WaitForSeconds(3.0f);
        subs.text = "";
        yield return new WaitForSeconds(1.0f);
        subs.text = "Eles podiam achar que eu é que sou culpada. E a minha irmã?";
        yield return new WaitForSeconds(7.0f);
        subs.text = "";
        ///
        yield return new WaitForSeconds(1.0f);
        subs.text = "O que ela vai sentir?";
        yield return new WaitForSeconds(3.0f);
        subs.text = "";
        ///
        yield return new WaitForSeconds(1.0f);
        subs.text = "O que é que podia acontecer?";
        yield return new WaitForSeconds(3.0f);
        subs.text = "";
        ///
        yield return new WaitForSeconds(2.0f);
        subs.text = "Não lhes posso contar";
        yield return new WaitForSeconds(3.0f);
        subs.text = "";
        ///
    }

    public void Diana_Ethics_CallParents() => StartCoroutine(CallParents());
    private IEnumerator CallParents() {
        yield return new WaitForSeconds(0f);
        subs.text = "Foi tomada a decisão de chamar os pais à " +
            "consulta, com o consentimento da cliente, " +
            "ou pedir-lhe que fale com eles, no sentido de " +
            "decidirem em conjunto o que fazer";
        yield return new WaitForSeconds(8f);
        subs.text = "";
        ///
        yield return new WaitForSeconds(0.5f);
        subs.text = "Esta ação deverá ser tão mais rápida" +
            " consoante o perigo de exposição ao agressor seja maior";
        yield return new WaitForSeconds(5.0f);
        subs.text = "";
        ///
    }

    public void Diana_Ethics_CallComission() => StartCoroutine(CallComission());
    private IEnumerator CallComission() {
        yield return new WaitForSeconds(0f);
        subs.text = "Na ausência de consentimento da cliente para contacto com a CPCJ, " +
            "esta decisão poderá ser tomada, se e só se a situação de perigo for evidente e a capacidade" +
            " de a cliente lidar com a situação for claramente insuficiente";
        yield return new WaitForSeconds(14f);
        subs.text = "";
        yield return new WaitForSeconds(0f);
        subs.text = "Claro que o envolvimento dos pais (que será sempre necessário com a entrada CPCJ) poderá ser negociado novamente";
        yield return new WaitForSeconds(8f);
        subs.text = "";
        yield return new WaitForSeconds(0f);
        subs.text = "Mas, nestes casos, onde o profissional de psicologia perceba que a relação" +
            " com os pais poderá não ser suficientemente protetora, a CPCJ " +
            "terá que ser equacionada como alternativa";
        yield return new WaitForSeconds(12.5f);
        subs.text = "";
        yield return new WaitForSeconds(0f);
        subs.text = "Este encaminhamento para a CPCJ poderá igualmente ser realizado através da escola";
        yield return new WaitForSeconds(5f);
        subs.text = "";
        ///
    }

    public void Diana_Ethics_Talk() => StartCoroutine(EthicsTalk());
    private IEnumerator EthicsTalk() {
        yield return new WaitForSeconds(0f);
        subs.text = "Esta escolha advém da discussão com a cliente" +
            " sobre alternativas ao envolvimento dos pais que possam " +
            "ser protetoras de, no limite, continuidade da situação de perigo";
        yield return new WaitForSeconds(10f);
        subs.text = "";
        yield return new WaitForSeconds(0f);
        subs.text = "O envolvimento da Comissão de Proteção de Crianças" +
            " e Jovens deve ser discutido com a cliente, em função" +
            " de garantir um contexto seguro para a mesma";
        yield return new WaitForSeconds(8.5f);
        subs.text = "";
        yield return new WaitForSeconds(0f);
    }

    public void Instructions_Feed1_Exp() => StartCoroutine(Feed1_Exp());
    private IEnumerator Feed1_Exp() {
        yield return new WaitForSeconds(0);
        subs.text = "O quão inovativa considerou ser esta experiência?";
        yield return new WaitForSeconds(3);
        subs.text = "";
        yield return new WaitForSeconds(0);
    }

    public void Instructions_Feed2() => StartCoroutine(Feed2());
    private IEnumerator Feed2() {
        yield return new WaitForSeconds(0);
        subs.text = "Sensação que esteve fisicamente dentro do ambiente virtual?";
        yield return new WaitForSeconds(24);
        subs.text = "";
        yield return new WaitForSeconds(0);
    }

    public void Instructions_Feed3() => StartCoroutine(Feed3());
    private IEnumerator Feed3() {
        yield return new WaitForSeconds(0);
        subs.text = "O quanto sente ter aprendido com esta experiência?";
        yield return new WaitForSeconds(05f);
        subs.text = "";
        yield return new WaitForSeconds(0);
    }

    public void Instructions_Feed4() => StartCoroutine(Feed4());
    private IEnumerator Feed4() {
        yield return new WaitForSeconds(0);
        subs.text = "Nível de satisfação com a experiência e com o seu desempenho na tarefa?";
        yield return new WaitForSeconds(5.5f);
        subs.text = "";
        yield return new WaitForSeconds(0);
    }

    public void Instructions_Feed5() => StartCoroutine(Feed5());
    private IEnumerator Feed5() {
        yield return new WaitForSeconds(0);
        subs.text = "Sensação de tonturas, náuseas, vómitos, ou desconforto ocular?";
        yield return new WaitForSeconds(7f);
        subs.text = "";
        yield return new WaitForSeconds(0);
    }

    public void Instructions_Thanks() => StartCoroutine(Thanks());
    private IEnumerator Thanks() {
        yield return new WaitForSeconds(0);
        subs.text = "Obrigado pela sua participação";
        yield return new WaitForSeconds(2f);
        subs.text = "";
        yield return new WaitForSeconds(0);
    }
}
